'''
Created on Aug 4, 2018
File for parsing excel and take necessary data
@author: Maksim Golikov
'''
import os
import  pandas as pd


#Position of cells
NAME_CELL           = 0
TYPE_CELL           = 1
DEFAULT_VAL_CELL    = 2
MIN_VAL_CELL        = 3
MAX_VAL_CELL        = 4
QUONTITY_VAL_CELL   = 5

TYPE_DESCRIBE_NAME_CELL = 0 
TYPE_DESCRIBE_BYTE_CELL = 1

SHEET_WITH_NAME = 0
SHEET_WITH_TYPES = 1

class FiledDB(object):
    '''
    class for describe field of excel and parsing it correctly
    '''
    def __init__(self, name, ftype, defaultValue, minvalue, maxValue, quontity):
        self.name          = name
        self.type          = ftype
        self.default_value = defaultValue
        self.min_value     = minvalue
        self.max_value     = maxValue
        self.quontity      = quontity
        
       

  
    
#return -1 if operation contain same error, else return 
def GetListOfDataFrom(fileName):
        
    if os.path.isfile(fileName):
        DataFromExcel=[]
        uSheet    = pd.read_excel(fileName, sheet_name= SHEET_WITH_NAME )
        exRows    = len(uSheet)
        
        for row in range(0, exRows):
            f_name     = str(uSheet.iat[row,NAME_CELL])
            f_type     = str(uSheet.iat[row,TYPE_CELL])
            f_default  = str(uSheet.iat[row,DEFAULT_VAL_CELL])
            f_min      = str(uSheet.iat[row,MIN_VAL_CELL])
            f_max      = str(uSheet.iat[row,MAX_VAL_CELL])
           
            if str(uSheet.iat[row,QUONTITY_VAL_CELL] ) != 'nan':
                f_quontity = str(uSheet.iat[row, QUONTITY_VAL_CELL])
            else:
                f_quontity = 1
        
            newObj = FiledDB(f_name, f_type, int(float(f_default)), int(float(f_min)), int(float(f_max)), int(float(f_quontity)) )
            DataFromExcel.append(newObj)

        return DataFromExcel
    else:
        print("File absent")    
        
    
#return -1 if operation contain same error, else return 
def GetListOfTypeDescriptionFrom(fileName):
        
    if os.path.isfile(fileName):
        DataFromExcel={}
        uSheet    = pd.read_excel(fileName, sheet_name= SHEET_WITH_TYPES )
        exRows    = len(uSheet)
    
        for row in range(0, exRows):
            f_name       = str(uSheet.iat[row, TYPE_DESCRIBE_NAME_CELL])
            f_byteCount  = str(uSheet.iat[row, TYPE_DESCRIBE_BYTE_CELL])  
                        
            DataFromExcel[f_name] = int(float(f_byteCount))
        
        return DataFromExcel
    else:
        print("File absent") 
        
    